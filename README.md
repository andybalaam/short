# short

`short` is a command that truncates each input line to fit in a specified width.

Examples:

```sh
$ grep foo myfile.txt | short
... lots of output here ...
... but none of it longer than a single line ...
```

```sh
$ head -n 3 /etc/passwd | short -w 5
root:
bin:x
daemo
```

By default, short makes lines fit into your terminal window, or 80 columns if
you are not in an interactive terminal.

Full help text:

```
short 0.1.0

USAGE:
    short [OPTIONS] [FILE]...

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
    -w, --width <width>    Use <width> columns instead of terminal width or 80 if unknown

ARGS:
    <FILE>...    Files to process
```

## Developing

Ensure you have [Rust](https://www.rust-lang.org/tools/install) installed.
`short` uses Rust 2018 edition.

To build the code:

```bash
cargo fmt
cargo test
```

## Installing

At the moment, you need Rust to install.  When you have it, you can install
`short` like this:

```bash
cargo install --path .
```

## Packagers wanted

Please create an issue in GitLab if you would like to package `short` for an
operating system or distribution.  We would be very happy to help any way we
can.

## License

`short` is Copyright 2020 Andy Balaam and the contributors, released under
[AGPLv3](LICENSE) or later.
