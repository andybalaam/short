use std::fs::File;
use std::io::{self, Read, StdoutLock, Write};
use std::path::PathBuf;
use structopt::StructOpt;
use terminal_size::{terminal_size, Width};

#[derive(StructOpt, Debug)]
#[structopt(name = "short")]
struct Options {
    #[structopt(
        short,
        long,
        help = "Use <width> columns instead of terminal width or 80 if unknown"
    )]
    width: Option<u16>,

    /// Files to process
    #[structopt(name = "FILE", parse(from_os_str))]
    files: Vec<PathBuf>,
}

fn guess_width() -> u16 {
    if let Some((Width(w), _)) = terminal_size() {
        w
    } else {
        80
    }
}

fn process_reader(
    file: &mut dyn Read,
    width: u16,
    eol: u8,
    stdout: &mut StdoutLock,
) -> io::Result<()> {
    let mut col = 0;
    for b in file.bytes() {
        let byte = b.unwrap();
        if col < width {
            stdout.write_all(&[byte])?;
        } else if col == width {
            stdout.write_all(&[eol])?;
        }
        col += 1;
        if byte == eol {
            col = 0;
        }
    }

    Ok(())
}

fn process_file(
    file_name: PathBuf,
    width: u16,
    eol: u8,
    stdout: &mut StdoutLock,
) -> io::Result<()> {
    if file_name == PathBuf::from("-") {
        let stdin_unlocked = io::stdin();
        process_reader(&mut stdin_unlocked.lock(), width, eol, stdout)?;
    } else {
        let mut file = File::open(file_name)?;
        process_reader(&mut file, width, eol, stdout)?;
    }

    Ok(())
}

fn main() -> io::Result<()> {
    let opt = Options::from_args();

    let stdout_unlocked = io::stdout();
    let mut stdout = stdout_unlocked.lock();

    let eol = '\n' as u8;
    let width = opt.width.unwrap_or_else(guess_width);

    let file_names = if opt.files.len() == 0 {
        vec![PathBuf::from("-")]
    } else {
        opt.files
    };

    for file_name in file_names {
        process_file(file_name, width, eol, &mut stdout)?;
    }

    Ok(())
}
